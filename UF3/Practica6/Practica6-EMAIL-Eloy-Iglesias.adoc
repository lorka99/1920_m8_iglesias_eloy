= M8

=== Practica 6: Práctica servidor Email

==== 1.Preparativos

* Utilizaremos el entorno de red local creada en la practica anterior.

* La maquinas tendran que tener el promp como las del anterior ejercicio.

==== Apartado 1:Configuración del servidor DNS a la red local.

Crearemos

* Registro SOA
* Registro NS
* Registro A de *Odin*
* Registro A de *Loki*
* Registro MX
* Registro CNAME


image::UF3/images/Practica6/1.PNG[]
image::UF3/images/Practica6/2.PNG[]
image::UF3/images/Practica6/3.PNG[]

==== Apartado 2:Preparación de la máquina loki.

image::UF3/images/Practica6/4.PNG[]

==== Apartado 3:Instalador del servicio de correo a loki.

* Instalamos postfix
* Durante la instalación seguimos lo básico del servidor
* En la opcion de tipo de servidor pondremos *Internet Site*
* Ponemos nuestro dominio

image::UF3/images/Practica6/5.PNG[]

==== Apartado 4:Comprobación de la configuración en local.

image::UF3/images/Practica6/6.PNG[]

==== Apartado 5:Comprobación del servidor de correo desde la red local.

Nos conectamos via telnet con la comanda 

*telnet loki.nombre.cognombre.local smtp*

image::UF3/images/Practica6/7.PNG[]

Envia un mensaje via telnet

image::UF3/images/Practica6/8.PNG[]

==== Apartado 6:Configuración de un certificado SSL.

Creamos un certicicado con la comanda 

*sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/mail.key -out /etc/ssl/certs/mailcert.pem*

image::UF3/images/Practica6/9.PNG[]

==== Apartado 7:Configuración del Postfix - servicio submission.

* Configuraremos el archivo */etc/postfix/master.cf*

Y pondremos esto

*submission inet n       -       -       -       -       smtpd
  -o syslog_name=postfix/submission
  -o smtpd_tls_wrappermode=no
  -o smtpd_tls_security_level=encrypt
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_relay_restrictions=permit_mynetworks,permit_sasl_authenticated,reject
  -o milter_macro_daemon_name=ORIGINATING
  -o smtpd_sasl_type=dovecot
  -o smtpd_sasl_path=private/auth*
  
image::UF3/images/Practica6/10.PNG[]

==== Apartado 8:Configuración del Postfix - opciones generales.

* En el archivo *main.cf* pondremos esto

*myhostname = mail.domain.com
myorigin = /etc/mailname
mydestination = mail.domain.com, domain.com, localhost, localhost.localdomain
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = ipv4*

I modificaremos solo *myhostname*, *mydestination* y *mynetworks* con nuestros datos

image::UF3/images/Practica6/11.PNG[]

* Configuramos los álias, añadiremos al archivo esto

*alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases*

image::UF3/images/Practica6/12.PNG[]

* Configuraremos el SSL poniendo esto en el archivo

*smtpd_tls_cert_file=/etc/ssl/certs/mailcert.pem
smtpd_tls_key_file=/etc/ssl/private/mail.key
smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
smtpd_tls_security_level=may
smtpd_tls_protocols = !SSLv2, !SSLv3*

image::UF3/images/Practica6/13.PNG[]

* Por ultimo añadimos

*local_recipient_maps = proxy:unix:passwd.byname $alias_maps*

image::UF3/images/Practica6/14.PNG[]

==== Apartado 9:Configuración de los àlias.

* Configuraremos el archivo */etc/aliases* y pondremos esto

*mailer-daemon: postmaster
postmaster: root
nobody: root
hostmaster: root
usenet: root
news: root
webmaster: root
www: root
ftp: root
abuse: root
root: elteuusuari*

Modificamos lo nuestro y despues ejecutamos la comanda *newaliases* para que el postfix vea los cambios

image::UF3/images/Practica6/15.PNG[]

==== Apartado 10:Configuración del protocolo IMAP.

* Instalamos los paquetes necesarios con la comanda

*apt-get install dovecot-core dovecot-imapd*

* Modificaremos el archivo */etc/dovecot/dovecot.conf* y pondremos esto

*disable_plaintext_auth = no
mail_privileged_group = mail
mail_location = mbox:~/mail:INBOX=/var/mail/%u
userdb {
  driver = passwd
}
passdb {
  args = %s
  driver = pam
}
protocols = "imap"*

*service auth {
  unix_listener /var/spool/postfix/private/auth {
    group = postfix
    mode = 0660
    user = postfix
  }
}*

*ssl=required
ssl_cert = </etc/ssl/certs/mailcert.pem
ssl_key = </etc/ssl/private/mail.key*

image::UF3/images/Practica6/16.PNG[]

Y para activarlo pondremos

*newaliases*

*postfix restart*

*systemctl restart dovecot*

==== Apartado 12:Configuración de un cliente gráfico.

* Instalaremos *thunderbird*

* Configuramos con nuestros datos

image::UF3/images/Practica6/22.PNG[]