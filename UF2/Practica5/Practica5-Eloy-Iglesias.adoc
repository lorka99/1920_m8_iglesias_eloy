= M8

=== Practica 5: Práctica servidor HTTP

==== 1.Preparativos

* Utilizaremos el entorno de red local creada en la practica anterior.

* La maquinas tendran que tener el promp como las del anterior ejercicio.

==== Apartado 1:Intalar el Apache a Tyr.

* Instalaremos el paquete *apache2* a *Tyr* y comprueba que ja se puede acceder a la red local.Para comprobar el funcionamiento del
entorno gráfico, podremos usar los navegadores de línia de comandas *lynx* o *w3m*

image::UF2/images/Practica5/1.PNG[]
image::UF2/images/Practica5/2.PNG[]

* Finalmente nos conectaremos en los puertos 80 y 443 a *Heimdall*. Para eso utilizaremos *iptables*.

image::UF2/images/Practica5/3.PNG[]

* Una vez echo ja podemos comprobar la conexión a la web de defecto desde la red del centro.

image::UF2/images/Practica5/4.PNG[]

==== Apartado 2:Creación de hosts virtuales.

* En este apatado daremos dos nombres de hosts diferentes. Por un lado tendremos el nombre de defecto
puesto anteriormente, *nombre.cognombre.local*. Y añadiremos el host *cognombre.nombre.local*.
Para hacer esto tendremos que:

1. Configurar el servidor DNS para que las peticiones de los dos dominios se dirigen correctamente a la IP pública de *Heimdall*.

2. Configurar el Apache para que distinga los dos nombres y muestre la página correcta en cada caso.

* La primera parte será añadir el servidor DNS el entorno de prácticas, requerirá la creación de la zona y de los registros necesarios al bind9 de *Odin*.

image::UF2/images/Practica5/5.PNG[]
image::UF2/images/Practica5/6.PNG[]
image::UF2/images/Practica5/7.PNG[]

Vamos a crear ahora las páginas web del dominio.

* Crearemos la estructura de directorios necesaria. La web del dominio *nombre.cognombre.local*
se leera desde el directorio */var/www/nombre*, mientras que la web del dominio *cognombre.nombre.local*
se leera desde el directorio *var/www/cognombre*.

* Crear dos archivos *index.html*, a cada uno de los directorios anteriores. Escribe un codigo HTML a cada uno de ellos
de manera que se vea claramente a que host virtual nos conectamos.

image::UF2/images/Practica5/8.PNG[]
image::UF2/images/Practica5/9.1.PNG[]
image::UF2/images/Practica5/9.2.PNG[]
 
* Crear los archivos de configuración del Apache, una por cada host virtual. Utiliza el modelo del
archivo */etc/apache2/sites-available/000-default.conf*. No te olvides de configurar
las directivas *ServerName* y *DocumentRoot*.
 
image::UF2/images/Practica5/10.1.PNG[]
image::UF2/images/Practica5/10.2.PNG[]
 
* Activa los dos hosts virtuales que hamos creado y desactiva el host por defecto.

image::UF2/images/Practica5/11.PNG[]
image::UF2/images/Practica5/12.PNG[]

* Reinicia el Apache utilizando la orden *apachectl*

* Comprueba que puedes acceder a cada uno de los hosts de tu ordenador.

image::UF2/images/Practica5/13.1.png[]
image::UF2/images/Practica5/13.2.png[]

==== Apartado 3:Configuración del primer host virtual.

* Las sigüientes configuraciónes se han de aplicar en el site *nombre.cognombre.local*. Recuerda
que siempre hace falta reiniciar el Apache para que los cambios hagan efecto.

* Cambiar el nombre del archivo *index.html* a *inici.html*. Haz que sea el archivo por defecto.

image::UF2/images/Practica5/14.png[]

* Utiliza la directiva *ServerAlias* para hacer que si el usuario utilize el nombre *www.nombre.cognombre.local* en lugar de *nombre.cognombre.local*.

* Para que todo esto funcione, el servidor DNS tiene que resolver la dirección
*www.nombre.cognombre.local* a tu dirección IP.

image::UF2/images/Practica5/15.png[]
image::UF2/images/Practica5/16.png[]

* Crear un tercer directorio dentro de */var/www* que se llame *test* y poner
un archivo *inici.html* dentro que muestre claramente que lugar estas visitando.
Después, utiliza la directiva *Alias* para hacer que la dirección *nombre.cognombre.local/test* vaya a parar a este directorio.

image::UF2/images/Practica5/17.png[]
image::UF2/images/Practica5/18.png[]

* Configurar el Apache para que permita el uso de archivos *.htaccess* en el directorio */var/www/test*, pero asegurate que no pueden los demas directorios.

image::UF2/images/Practica5/19.png[]

* Utiliza el archivo *.htaccess* para acceder al directorio */var/www/test* proporcionando un nombre de usuario y una contraseña válidos. Tendria que haberdos usuarios con permiso de acceso: *nombre* y *cognombre*; los 2 válidos con contraseña *12345*

image::UF2/images/Practica5/20.png[]
image::UF2/images/Practica5/21.png[]

==== Apartado 4:Configuración del segundo host virtual.

Las sigüientes configuraciones se tienen que aplicar al site *cognombre.nombre.local*.

Queremos que este site vaya siempre por HTTPS en lugar de HTTP. Por eso seguiremos los sigüientes pasos:

* Copia el archivo */etc/apache2/sites-available/default-ssl.conf* como modelo.

* Modifica la copia con los datos adecuados para tu host. Puedes utilizar el mismo certificado SSL que utilizaste a la práctica FTP o generar uno de nuevo.

image::UF2/images/Practica5/22.png[]

* Una vez modificada la configuración, activa el nuevo site.

image::UF2/images/Practica5/23.png[]

* Comprueba con el navegador que podemos acceder tanto por HTTP como por HTTPS a este site.

image::UF2/images/Practica5/24.1.png[]
image::UF2/images/Practica5/24.2.png[]

* Forcaremos ahora que cualquer petición HTTP se transforme autimaticamente a HTTPS. Para eso añadiremos las sigüientes línias a la configuración del site HTTP:

[red]#RewriteEngine on
RewriteCond %{SERVER_NAME} =cognom.nom.lan [OR]
RewriteCond %{SERVER_NAME} =*.cognom.nom.lan
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,QSA,R=permanent]#

Con esto le estamos pidiendo al Apache que reescriba cualquier petición dirigida a *cognombre.nombre.local*, o a cualquier subdominio suyo, a la misma dirección que se a pedido, pero con HTTPS.

* Comprueba con el navegador que si accedemos ahora por HTTP, la petición se modifica automaticamente.

image::UF2/images/Practica5/25.png[]

==== Apartado 5:Activación de páginas personales para los usuarios.

Activa las páginas personales de cada usuario del sistema tal como se esxplica en loas apuntes de teoria. Haz que l directorio *public* de los usuarios se pueda utilizar para guardar su página personal, que será visible en la dirección *http://nombre.cognombre.local/~usuari/*.

Utiliza la sigüiente regla de reescritura para sustituir el símbolo ~ de las direcciones para *users*, de manera que para acceder a la página personal del usuario *nombre* se pueda hacer a través de *http://nombre.cognombre.local/users/nombre*.

 [red]#RewriteEngine on
RewriteRule ^/users/(.*)$ /~$1 [R]#

image::UF2/images/Practica5/26.png[]

==== Apartado 6:Instalación de un LAMP.

Sigue las instrucciones de los apuntes de la teoría para conseguir que el servidor Apache tenga soporte por PHP y se pueda conectar a una base de datos

image::UF2/images/Practica5/27.png[]
image::UF2/images/Practica5/28.PNG[]
