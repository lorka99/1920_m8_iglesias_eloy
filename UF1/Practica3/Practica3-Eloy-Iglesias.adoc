= M8

=== Practica 3: Práctica servidor DNS

==== 1.Preparativos

* Utilizaremos el entorno de red local creada en la practica anterior.

* Configuraremos la máquina *Odin* como servidor DNS principal y *Frigga* como DNS secundario como router el *Thor* y el *Heimdall*.

* La maquinas tendran que tener el promp como las del anterior ejercicio.

==== Apartado 1:Instalación del servidor DNS a Odin.

En primer ligar será comprobar la configuración DNS actual de *Odin* tendremos que hacer un [red]#nslookup# o un [red]#dig# de [red]#louvre.fr#.

image::UF1/images/Practica3/1.png[]

* Instalaremos el servidor DNS a *Odin*

image::UF1/images/Practica3/2.png[]

* Comprobaremos que el servidor DNS está en ejecución con la comanda [red]#netstat#

image::UF1/images/Practica3/3.png[]

Una vez instalado el servidor DNS podremos modificar la configuración de la red de *Odin*.

* Modificaremos el archivo [red]#/etc/systemd/resolve.conf# y escribimos la dirección IP de *Odin*.

* Reiniciar la máquina después de hacer los cambios para asegurar que se efectúan.

image::UF1/images/Practica3/4.png[]

Antes de probar el DNS, el programa [red]#rndc# se utiliza para controlar la ejecución del [red]#bind# 

* Utilizaremos [red]#rndc# para activar la depuración del servidor, pondrmos la depuración de nivel 3

image::UF1/images/Practica3/5.png[]

Como en nuestra red no se utiliza IPv6 indicaremos al servidor que solo trabaje en IPv4.

Para hacer que el servidor solo trabaje en IPv4 tendremos que modificar el archivo [red]#/etc/default/bind9# y cambiaremos la línia de los [red]#OPTIONS# de [red]#OPTIONS="-u bind"# a [red]#OPTIONS="-4 -u bind"#.

image::UF1/images/Practica3/6.png[]

Ahora el objetivo será reducir los mensajes que no queremos del registr, para ello deshabilitaremos la verificación dnssec.

Para ello necesitaremos modificar el archivo [red]#named.conf.options# y modificamos la línia [red]#dnssec-validation auto# a [red]#dnssec-validation no#

Finalmente para que el registro muestre las consultas realizadas y su origen, añadiremos al archivo [red]#named.conf.options# la línia [red]#querylog yes;#.

Como siempre después de realizar una modificación reiniciar el servidor DNS.

image::UF1/images/Practica3/7.png[]

* Consultar la IP de un nuevo dominio por ejemplo britishmuseum.org

image::UF1/images/Practica3/8.png[]

* Comprovar la información de depuración que ha quedado al registro 
[red]#bind#.

image::UF1/images/Practica3/9.png[]

* Vuelca la memoria cau del servidor DNS utilizando [red]#rndc#

image::UF1/images/Practica3/10.png[]

* Ahora desactivaremos la depuración ([red]#rndc#)

image::UF1/images/Practica3/11.png[]

==== Apartado 2:Configuración de un servidor DNS solo en caché.

En este apartado configuraremos el servidor DNS para que actue como un cau DNS.

Canvios a hacer en [red]#/etc/bind/named.conf.options#:

* Configurar el servidor para que no resuelva peticiones IPv6.

* Crear una lista de control de acceso para que solo los clientes de nuestra res utilizen el servidor DNS.

* Activar la recursión y permitir que los clientes definidos en la ACL anterior puedan hacer consultas.

* No permitir la transferéncia de toda la información del DNS a nadie.

image::UF1/images/Practica3/12.png[]

==== Apartado 3:Configuración de un servidor DNS forwarding.

Canvios a hacer en [red]#/etc/bind/named.conf.options#:

* Configurar los forwarders para hacer que las peticiones externas se junten con los mismos servidores de nuestra red.

* Indicar al servidor que solo utilize el forwarding y que no intentar resolver las consultas directamente.

* Activar la validación segura de las respuestas externas.

image::UF1/images/Practica3/13.png[]

* Utilizar [red]#named-checkconf#, si no muestra respuesta és que no tiene ningun error.

image::UF1/images/Practica3/14.png[]

==== Apartado 4:Configuración de los clientes.

* Configurar *Odín,Frigga,Heimdall y Thor* para que utilizen *Odín y Frigga* como servidores DNS.

* Configurar el servidor DHCP para que los clientes con IP dinàmica también utilizen nuestros servidores DNS.

*Nuestro dominio se llamara "eloy.iglesias.local"*

* Configurar el servidor DHCP para que los clientes que busquen por defecto nuestro dominio.

image::UF1/images/Practica3/15.png[]

==== Apartado 5:Creación de la zona interna.

Crearemos nombres lógicos para *Odín* (dhcp1.eloy.iglesias.local) y para *Tyr* (www.eloy.iglesias.local)

La configuración de una zona nueva siempre conlleva 4 partes:

1 - Añadir configuración general en [red]#named.conf.local#.

2 - Añadir resolución directa.

3 - Añadir resolución inversa.

4 - Comprovar la configuración.

image::UF1/images/Practica3/16.png[]

==== Apartado 6:Configuración de la resolución directa.

Crearemos un archivo de resolución directa en los cuales tendremos:

* El registro SOA

* Configurar los registros NS

* Añadir los registros A y CNAME

* Añadir registro MX (para el futuro)

image::UF1/images/Practica3/17.png[]

==== Apartado 7:Configuración de la resolución inversa.

Crearemos un archivo de resolución inversa en los cuales tendremos:

* El registro SOA

* Configurar los registros NS

* Añadir los registros PTR

image::UF1/images/Practica3/18.png[]

==== Apartado 8:Conprovación de la configuración.

image::UF1/images/Practica3/19.png[]

==== Apartado 9:Configuración del servidor DNS secundario.

Tendremos que:

* Instalar bind en *Frigga*

* Editar [red]#/etc/bind/named.conf.options# de *Frigga*

* Editar [red]#/etc/bind/named.conf.local#

image::UF1/images/Practica3/20.png[]

Comprovación de *Frigga* en PC1

image::UF1/images/Practica3/21.png[]